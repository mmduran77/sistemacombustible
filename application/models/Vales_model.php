<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vales_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE vales');
		return $this->db->insert("vales",$data);
	}

	public function update($data,$id){
		$this->db->where("id_vales",$id);
		return $this->db->update("vales",$data);
	}

	public function getVale($id){
		$this->db->select("v.*");
		$this->db->from("vales v");
		$this->db->where("v.id_vales",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getVales(){
		$this->db->select("v.*");
		$this->db->from("vales v");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_vales", $id);
		$this->db->db_debug = false;
		if($this->db->delete("vales")){
			return array("success","Se eliminó correctamente!");
		}else{
			return array("error","No se puede eliminar un cliente activo!");
		}
	}

}