<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distancia_model extends CI_Model {


    public function save($data){
		$this->db->query('ALTER TABLE distancias');
		return $this->db->insert("distancias",$data);
	}

	public function update($data,$id){
		$this->db->where("id_distancia",$id);
		return $this->db->update("distancias",$data);
	}

	public function getDistancia($id){
		$this->db->select("d.*");
		$this->db->from("distancias d");
		$this->db->where("d.id_distancia",$id);
		$result = $this->db->get();
		return $result->row();
	}
	
	public function getDistancias(){
		$this->db->select("d.*");
		$this->db->from("distancias d");
		$results = $this->db->get();
		return $results->result();
	}

	public function delete($id){
		$this->db->where("id_distancia", $id);	
	    $this->db->delete("distancias");
			
	}

}