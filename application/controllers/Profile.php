<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Profile_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav');
        $this->load->view('profile');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/profile');
    }

    public function save(){

		$idUser	= $this->session->userdata("id");
		$nombre = $this->input->post("nombre");
		$apellido  = $this->input->post("apellido");
        $usuario  = $this->input->post("usuario");
        $validate_usuario = "";
        
        if($usuario!=$this->session->userdata("usuario")){
            $validate_usuario = "|is_unique[usuarios.usuario]";
        }

        $this->form_validation->set_rules("nombre","nombre","required|min_length[3]");
        $this->form_validation->set_rules("usuario","usuario","required|valid_email".$validate_usuario);
        $this->form_validation->set_rules("apellido","apellido","required");
        
		if ($this->form_validation->run()==TRUE) {
    
			$data  = array(
                'nombre' => $nombre,
                'apellido' => $apellido,
                'usuario' => $usuario,
			);

            $this->Profile_model->save($data,$idUser);
            redirect(base_url()."Signin/UpdateSession");
		}else{
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav');
            $this->load->view('profile');
            $this->load->view('layout/footer');
            $this->load->view('layout/js/profile');
		}

    }
    
    public function upload(){

        $id    = $this->session->userdata("id");
		$picture   ="img".$id.".png";

		$config['upload_path'] 		 = './assets/img/user';
		$config['allowed_types'] = 'jpg|png';
		$config['overwrite']   = true;
		$config['file_name']  = $picture;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('picture')) {

			$resp= array('type' => "error",'message' => substr($this->upload->display_errors(), 3, -4));

		} else {
            $this->upload->data();
			$resp= array('type' => "success", 'message' => "La imagen se subió correctamente");
		}

		echo json_encode($resp);

	}
}
