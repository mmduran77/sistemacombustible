<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Vehiculo_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index($id)
	{   

        $data = $this->Vehiculo_model->getVehiculo($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->session->set_userdata('id_vehiculo', $id);
        
        if($data){
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav');
            $this->load->view('vehiculo/editar',$data,$data2);
            $this->load->view('layout/footer');
            $this->load->view('layout/js/vehiculo');
        }

    }
    
    public function update($id){

        $marca = $this->input->post("marca");
        $modelo = $this->input->post("modelo");
        $patente = $this->input->post("patente");
        $tanque = $this->input->post("tanque");
        $usuarioactual=$this->session->userdata("nombre");
       
        

        $this->form_validation->set_rules("marca","marca","required|min_length[3]");
        $this->form_validation->set_rules("modelo","modelo","required");
        $this->form_validation->set_rules("patente","patente","required");
        $this->form_validation->set_rules("tanque","tanque","required");

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
                'marca' => $marca,
                'modelo' => $modelo,
                'patente' => $patente,
                'tanque' => $tanque
                'modificacion' => date("Y-m-d h:i:s")
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar Vehiculo',
				'descripcion'=>'el usuario '.$usuarioactual.' edito Vehiculo: '.$patente.'',

			);
			$this->Log_model->save($data2);

			$this->Vehiculo_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."vehiculos");
            
		}else{
			$this->index($id);
		}
    }
  
}
