<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Ordenes_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
        }
        
    }

	public function index()
	{       
        $data = array("data" => $this->Ordenes_model->getOrdenes());
        $data2 = array("data2" => $this->Usuario_model->getMensajes());  

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('ordenes/lista',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/ordenes');
    }
    
    public function delete($id){
            
        $resp=$this->Ordenes_model->delete($id);
        $this->session->set_flashdata($resp[0],$resp[1]);
        redirect(base_url()."ordenes");
                    
    }

    public function getDataOrigen(){
        $resp = $this->Ordenes_model->getOrigen();
        if($resp){
            echo json_encode($resp);
        }
    }
    public function getDataCliente(){
        $resp = $this->Ordenes_model->getCliente();
        if($resp){
            echo json_encode($resp);
        }
    }
    public function getDataVehiculo(){
        $resp = $this->Ordenes_model->getVehiculo();
        if($resp){
            echo json_encode($resp);
        }
    }
    public function getDataConductor(){
        $resp = $this->Ordenes_model->getConductor();
        if($resp){
            echo json_encode($resp);
        }
    }


}
