<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model("Distancia_model");
		$this->load->model("Usuario_model");
		$this->load->model("Log_model");
		if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   
		$data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('Distancia/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/distancia'); 
    }
    
	public function save(){

        $origen	= $this->input->post("origen");
        $destino = $this->input->post("destino");
		$km = $this->input->post("km");
		$usuarioactual=$this->session->userdata("nombre");
        

        $this->form_validation->set_rules("origen","origen","required");
        $this->form_validation->set_rules("destino","destino","required");
		$this->form_validation->set_rules("km","km","required");
      

		if ($this->form_validation->run()==TRUE) {
			$data  = array(
				'origen' => $origen,
                'destino' => $destino,
                'km' => $km,               
			);
			$data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar Distancia',
				'descripcion'=>'el usuario '.$usuarioactual.' agrego nueva Distancia desde: '.$origen.' hasta: '.$destino.'',

			);
			$this->Log_model->save($data2);

			$this->Distancia_model->save($data);

			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."distancias");
		}else{
			$this->load->view('layout/head');
			$this->load->view('layout/sidenav');
			$this->load->view('layout/topnav');
			$this->load->view('distancia/agregar');
			$this->load->view('layout/footer');
			//$this->load->view('layout/js/client');
		}
	}

	
}
