<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Localidades_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
    }

	public function index($id)
	{       
        $data = $this->Localidades_model->getLocalidad($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        if($data){

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav');
        $this->load->view('Localidades/editar',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/localidades');
        }
    }
    public function update($id){

        $localidad	= $this->input->post("localidad");
        $provincia = $this->input->post("provincia");
        $CP = $this->input->post("CP");
        $usuarioactual=$this->session->userdata("nombre");

        $this->form_validation->set_rules("localidad","localidad","required");
        $this->form_validation->set_rules("provincia","provincia","required");
		$this->form_validation->set_rules("CP","CP","required");
       

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'localidad' => $localidad,
                'provincia' => $provincia,
                'CP' => $CP
                
               'modificacion' => date("Y-m-d h:i:s")
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar Localidad',
				'descripcion'=>'el usuario '.$usuarioactual.' edito la Localidad: '.$localidad.'',

			);
			$this->Log_model->save($data2);

			$this->Localidades_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."localidades");
            
		}else{
			$this->index($id);
		}
	}
    

}
