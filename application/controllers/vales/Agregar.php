<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model("Vales_model");
		$this->load->model("Log_model");
		$this->load->model("Usuario_model");
		if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index()
	{   
		$data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('vales/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/vales'); 
    }
    
    public function save(){

		$comprobante = $this->input->post("comprobante");
		$litros = $this->input->post("litros");
		$importe = $this->input->post("importe");
		$usuarioactual=$this->session->userdata("nombre");

		$this->form_validation->set_rules("comprobante","localidad","required");
        $this->form_validation->set_rules("litros","provincia","required");
		$this->form_validation->set_rules("importe","CP","required");

		if ($this->form_validation->run()==TRUE) {
			$data  = array(
				'comprobante' => $comprobante,
                'litros' => $litros,
                'importe' => $importe,               
			);
			$data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar Vales',
				'descripcion'=>'el usuario '.$usuarioactual.' agrego nuevo vale: '.$comprobante.'',

			);
			$this->Log_model->save($data2);

			$this->Vales_model->save($data);

			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."vales");
		}else{
			$this->load->view('layout/head');
			$this->load->view('layout/sidenav');
			$this->load->view('layout/topnav');
			$this->load->view('vales/agregar');
			$this->load->view('layout/footer');
			$this->load->view('layout/js/vales');
		}
		
	
	}

	
}
