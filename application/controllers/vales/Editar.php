<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Vales_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
    }

	public function index($id)
	{       
        $data = $this->Vales_model->getVale($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        if($data){

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('vales/editar',$data);
        $this->load->view('layout/footer');
        $this->load->view('layout/js/vales');
        }
    }
    public function update($id){

        $comprobante	= $this->input->post("comprobante");
        $litros = $this->input->post("litros");
        $importe = $this->input->post("importe");
        $usuarioactual=$this->session->userdata("nombre");

        $this->form_validation->set_rules("comprobante","comprobante","required");
        $this->form_validation->set_rules("litros","litros","required");
		$this->form_validation->set_rules("importe","importe","required");
       

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'comprobante' => $comprobante,
                'litros' => $litros,
                'importe' => $importe
                
               'modificacion' => date("Y-m-d h:i:s")
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar Vales',
				'descripcion'=>'el usuario '.$usuarioactual.' Editar vale: '.$comprobante.'',

			);
			$this->Log_model->save($data2);

			$this->Vales_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."vales");
            
		}else{
			$this->index($id);
		}
	}
    

}
