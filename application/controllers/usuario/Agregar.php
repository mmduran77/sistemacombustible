<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agregar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Usuario_model");
        $this->load->model("Category_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index(){

        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $id=$this->Usuario_model->getId();
        $this->session->set_userdata('id', $id);

        $this->load->view('layout/head');
        $this->load->view('layout/sidenav');
        $this->load->view('layout/topnav',$data2);
        $this->load->view('usuarios/agregar');
        $this->load->view('layout/footer');
        $this->load->view('layout/js/user');
    }
    
    public function save(){

        $apellido	= $this->input->post("apellido");
        $nombre	= $this->input->post("nombre");
        $usuario = $this->input->post("usuario");
        $password = $this->input->post("password");
        $telefono = $this->input->post("telefono");
        $direccion = $this->input->post("direccion");

        $usuarioactual=$this->session->userdata("nombre");
       
		

        $this->form_validation->set_rules("apellido","apellido","required|min_length[5]|max_length[12]");
        $this->form_validation->set_rules("nombre","nombre","required|min_length[5]|max_length[12]");
        $this->form_validation->set_rules("usuario","usuario","required|is_unique[usuarios.usuario]");
        $this->form_validation->set_rules("password","password","trim|required|min_length[5]|max_length[12]");
        $this->form_validation->set_rules("telefono","telefono","required|numeric");
        $this->form_validation->set_rules("direccion","direccion","required|min_length[5]|max_length[12]");
        


		if ($this->form_validation->run()==TRUE) {
    
			$data  = array(
                'apellido' => $apellido,
				'nombre' => $nombre,
                'usuario' => $usuario,
                'password' => md5($password),
                'telefono' => $telefono,
                'direccion' => $direccion
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Agregar usuario',
				'descripcion'=>'el usuario '.$usuarioactual.' agrego nuevo usuario: '.$usuario.'',

			);
			$this->Log_model->save($data2);
           
            $this->Usuario_model->save($data);
           
			$this->session->set_flashdata("success","Se guardó correctamente!");
			redirect(base_url()."usuario");
		}else{
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav');
            $this->load->view('usuarios/agregar');
            $this->load->view('layout/footer');
            $this->load->view('layout/js/user');
		}
    }
    

	
}
