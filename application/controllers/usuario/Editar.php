<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Usuario_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index($id)
	{   

        $data = $this->Usuario_model->getUsuario($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        $this->session->set_userdata('id',$id);

        if($data){
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav',$data2);
            $this->load->view('usuarios/editar',$data);
            $this->load->view('layout/footer');
            $this->load->view('layout/js/user');
        }

    }
    
    public function update($id){

        $apellido	= $this->input->post("apellido");
        $nombre	= $this->input->post("nombre");
        $usuario = $this->input->post("usuario");
        $password = $this->input->post("password");
        $rol = $this->input->post("rol");
        $telefono = $this->input->post("telefono");
        $direccion = $this->input->post("direccion");
        $usuarioactual=$this->session->userdata("nombre");
        
        
        $data = $this->Usuario_model->getUsuario($id); 
        $validate_usuario = "";
        
        if($usuario!=$data->usuario){
            $validate_usuario = "|is_unique[usuarios.usuario]";
        }
        
        $this->form_validation->set_rules("apellido","apellido","required|min_length[5]|max_length[12]");
        $this->form_validation->set_rules("nombre","Nombre","required|min_length[5]|max_length[12]");
        $this->form_validation->set_rules("usuario","usuario","required".$validate_usuario);
        $this->form_validation->set_rules("password","password","required");
        $this->form_validation->set_rules("rol","rol","required");
        $this->form_validation->set_rules("telefono","telefono","required|min_length[8]|max_length[12]");
        $this->form_validation->set_rules("direccion","direccion","required|min_length[5]|max_length[12]");



		if ($this->form_validation->run()==TRUE) {
            
			$data  = array(
                'apellido' => $apellido,
				'nombre' => $nombre,
                'usuario' => $usuario,
                'password' => $password,
                'rol'=>$rol,
                'telefono' => $telefono,
                'direccion'=>$direccion,
                'modificacion' => date("Y-m-d h:i:s")
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar usuario',
				'descripcion'=>'el usuario '.$usuarioactual.' edito al usuario: '.$usuario.'',

			);
			$this->Log_model->save($data2);

			$this->Usuario_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."usuario");
            
		}else{
			$this->index($id);
		}
    }
  
}
