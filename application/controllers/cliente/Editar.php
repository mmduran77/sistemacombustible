<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editar extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model("Cliente_model");
        $this->load->model("Log_model");
        $this->load->model("Usuario_model");
        if (!$this->session->userdata("login")) {
			redirect(base_url()."login");
		}
	}

	public function index($id)
	{   

        $data = $this->Cliente_model->getClient($id); 
        $data2 = array("data2" => $this->Usuario_model->getMensajes()); 
        
        if($data){
            $this->load->view('layout/head');
            $this->load->view('layout/sidenav');
            $this->load->view('layout/topnav',$data2);
            $this->load->view('cliente/editar',$data);
            $this->load->view('layout/footer');
            $this->load->view('layout/js/client');
        }

    }
    
    public function update($id){

        $nombre	= $this->input->post("nombre");
        $apellido = $this->input->post("apellido");
        $direccion = $this->input->post("direccion");
        

        $this->form_validation->set_rules("nombre","Nombre","required");
        $this->form_validation->set_rules("apellido","apellido","required");
		$this->form_validation->set_rules("direccion","direccion","required");
       

		if ($this->form_validation->run()==TRUE) {

			$data  = array(
				'nombre' => $nombre,
                'apellido' => $apellido,
                'direccion' => $direccion,
                
                'modificacion' => date("Y-m-d h:i:s"),
            );
            $data2=array(
				'fecha'=>date("y-m-d"),
				'hora'=>date("h:i:s"),
				'evento'=>'Editar Cliente',
				'descripcion'=>'el usuario '.$usuarioactual.' Edito al cliente: '.$apellido.''.$nombre.'',

			);
			$this->Log_model->save($data2);

			$this->Cliente_model->update($data,$id);
            $this->session->set_flashdata("success","Se modificó correctamente!");
            redirect(base_url()."clientes");
            
		}else{
			$this->index($id);
		}
	}
}
