<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Nuevo</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>ordenes">Ordenes</a></li>
                <li class="breadcrumb-item active">Nuevo</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>ordenes" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">
    <div class="row">
    <div class="col-xl-12 order-xl-1">
        <div class="card">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-12">
                    <h3 class="mb-0">Ingrese datos </h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <form action="<?php echo base_url();?>nuevo-ordenes/save" method="POST">
            <div class="row">

                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-labe">Origen</label>
                                            <select id="origen" name="origenId" class="form-control">
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('origen'); ?></div>
                                        </div>
                            </div>
                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Destino</label>
                                            <select id="destino" name="destino" class="form-control">
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('destino'); ?></div>
                                        </div>
                            </div>
                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Estado</label>
                                            <select id="estado" name="estado" class="form-control">
                                            <option value="value1">En Preparacion</option>
                                            <option value="value2" selected>En viaje</option>
                                            <option value="value3">En Destino</option>
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('destino'); ?></div>
                                        </div>
                            </div>

            </div>
            <div class="row">

                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-labe">Cliente</label>
                                            <select id="cliente" name="cliente" class="form-control">
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('cliente'); ?></div>
                                        </div>
                            </div>
                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Vehiculo</label>
                                            <select id="vehiculo" name="vehiculo" class="form-control">
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('vehiculo'); ?></div>
                                        </div>
                            </div>
                            <div class="col-lg-4">
                                        <div class="form-group">
                                            <label class="form-control-label">Conductor</label>
                                            <select id="conductor" name="conductor" class="form-control">
                                            </select>
                                            <div class="invalid-feedback"><?php echo form_error('conductor'); ?></div>
                                        </div>
                            </div>
                            

            </div>
            <div class="form-group text-right">
                            <button type="submit" class="btn btn-success mt-4">Guardar</button>
                            </div>
            </form>
        </div>
        </div>
    </div>
    </div>
