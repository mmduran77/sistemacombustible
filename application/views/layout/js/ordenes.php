<script>
    $(document).ready( function () {
        $('#ordenesTable').DataTable();
        $(".nav-ordenes").addClass("active");
         origen();
         destino();
         cliente();
         vehiculo();
         conductor();
       
        
        <?php if($this->session->flashdata("success")):?>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: '<?php echo $this->session->flashdata("success"); ?>',
                showConfirmButton: false,
                timer: 2000
            })
        <?php endif; ?>

        <?php if($this->session->flashdata("error")):?>
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: '<?php echo $this->session->flashdata("error") ?>',
            })
        <?php endif; ?>

    });
    function origen() {
        $.ajax({
            url: "<?php echo base_url(); ?>ordenes/Lista/getDataOrigen",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id_distancia ==<?php echo set_value('origenId') ? set_value('origenId'): (!empty($origen_id) ? $origen_id: 0) ?>){
                        html.push('<option  value="'+value.id_distancia+'" selected>'+value.origen+'</option>');
                    }else{
                        html.push('<option  value="'+value.id_distancia+'">'+value.origen+'</option>');
                    }

                }); 

                $("#origen").html(html);

            }
        });
    }
    function destino() {
        $.ajax({
            url: "<?php echo base_url(); ?>ordenes/Lista/getDataOrigen",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id_distancia ==<?php echo set_value('destinoId') ? set_value('destinoId'): (!empty($origen_id) ? $origen_id: 0) ?>){
                        html.push('<option  value="'+value.id_distancia+'" selected>'+value.destino+'</option>');
                    }else{
                        html.push('<option  value="'+value.id_distancia+'">'+value.destino+'</option>');
                    }

                }); 

                $("#destino").html(html);

            }
        });
    }
    function cliente() {
        $.ajax({
            url: "<?php echo base_url(); ?>ordenes/Lista/getDataCliente",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id_cliente ==<?php echo set_value('cliente') ? set_value('cliente'): (!empty($cliente_id) ? $cliente_id: 0) ?>){
                        html.push('<option  value="'+value.id_cliente+'" selected>'+value.apellido+","+value.nombre+'</option>');
                    }else{
                        html.push('<option  value="'+value.id_cliente+'">'+value.apellido+","+value.nombre+'</option>');
                    }

                }); 

                $("#cliente").html(html);

            }
        });
    }
    function vehiculo() {
        $.ajax({
            url: "<?php echo base_url(); ?>ordenes/Lista/getDataVehiculo",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id_vehiculo ==<?php echo set_value('cliente') ? set_value('cliente'): (!empty($cliente_id) ? $cliente_id: 0) ?>){
                        html.push('<option  value="'+value.id_vehiculo+'" selected>'+value.patente+'</option>');
                    }else{
                        html.push('<option  value="'+value.id_vehiculo+'">'+value.patente+'</option>');
                    }

                }); 

                $("#vehiculo").html(html);

            }
        });
    }
    function conductor() {
        $.ajax({
            url: "<?php echo base_url(); ?>ordenes/Lista/getDataConductor",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id ==<?php echo set_value('conductor') ? set_value('conductor'): (!empty($conductor_id) ? $conductor_id: 0) ?>){
                        html.push('<option  value="'+value.id+'" selected>'+value.apellido+" , "+value.nombre+'</option>');
                    }else{
                        html.push('<option  value="'+value.id+'">'+value.apellido+" , "+value.nombre+'</option>');
                    }

                }); 

                $("#conductor").html(html);

            }
        });
    }
   

    
    
</script>