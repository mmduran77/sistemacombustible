<script>
    $(document).ready( function () {
        $('#chatTable').DataTable();
        $(".nav-chat").addClass("active");

       usuario();
        <?php if($this->session->flashdata("success")):?>
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: '<?php echo $this->session->flashdata("success"); ?>',
                showConfirmButton: false,
                timer: 2000
            })
        <?php endif; ?>

        <?php if($this->session->flashdata("error")):?>
            Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: '<?php echo $this->session->flashdata("error") ?>',
            })
        <?php endif; ?>

    });

    function usuario() {
        $.ajax({
            url: "<?php echo base_url(); ?>chat/getDataUsuario",
            type:"POST",
            dataType:"json",
            success:function(resp){


                var html=new Array();

                $.each(resp,function(key, value){
                    
                    if(value.id ==<?php echo set_value('usuario') ? set_value('usuario'): (!empty($usuario_id) ? $usuario_id: 0) ?>){
                        html.push('<option  value="'+value.usuario+'" selected>'+value.usuario+'</option>');
                    }else{
                        html.push('<option  value="'+value.usuario+'">'+value.usuario+'</option>');
                    }

                }); 

                $("#usuario").html(html);

            }
        });
    }

  

   
    
</script>