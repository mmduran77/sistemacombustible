<body>
  <!-- Sidenav -->
  <nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header  align-items-center">
        <a class="navbar-brand" href="javascript:void(0)">
          <img src="<?php echo base_url(); ?>assets/img/favicon.jpg" class="navbar-brand-img" alt="...">
          <br>
          Sistema Combustible
        </a>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
             <a class="nav-link nav-dashboard" href="<?php echo base_url(); ?>dashboard">
                <i class="ni ni-tv-2 text-default"></i>
                <span class="nav-link-text">Inicio</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-usuario" href="<?php  echo base_url(); ?>usuario">
                <i class="ni ni-single-02 text-default"></i>
                <span class="nav-link-text">Usuarios</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-client" href="<?php  echo base_url(); ?>clientes">
                <i class="ni ni-circle-08 text-default"></i>
                <span class="nav-link-text">Clientes</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-distancia" href="<?php  echo base_url(); ?>distancias">
                <i class="ni ni-map-big text-default"></i>
                <span class="nav-link-text">Distancia</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-vehiculo" href="<?php  echo base_url(); ?>vehiculos">
                <i class="ni ni-bus-front-12 text-default"></i>
                <span class="nav-link-text">Vehiculos</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-localidades" href="<?php  echo base_url(); ?>localidades">
                <i class="ni ni-building text-default"></i>
                <span class="nav-link-text">Localidades</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-vales" href="<?php  echo base_url(); ?>vales">
                <i class="ni ni-tag text-default"></i>
                <span class="nav-link-text">Vales</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-ordenes" href="<?php  echo base_url(); ?>ordenes">
                <i class="ni ni-cart text-default"></i>
                <span class="nav-link-text">Ordenes</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-viaticos" href="<?php  echo base_url(); ?>viaticos">
                <i class="ni ni-collection text-default"></i>
                <span class="nav-link-text">Viaticos</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link nav-chat" href="<?php  echo base_url(); ?>chat">
                <i class="ni ni-chat-round text-default"></i>
                <span class="nav-link-text">Chat</span>
              </a>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </nav>