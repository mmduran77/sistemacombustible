<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0"><?php echo $marca; ?></h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>vehiiculos">Vehiculos</a></li>
                <li class="breadcrumb-item active"><?php echo $marca; ?></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>vehiculos" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row">
        
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Actualice los datos</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                
                <form action="<?php echo base_url();?>vehiculo/update/<?php echo $id_vehiculo; ?>" method="POST">
                    <div class="form-group">
                        <label class="form-control-label">Marca</label>
                        <input type="text" name="marca" class="form-control <?php echo form_error('marca') ? 'is-invalid':''?>" placeholder="Ingrese marca del vehiculo"  value="<?php echo form_error('marca') ? set_value('marca'): $marca; ?>">
                        <div class="invalid-feedback"><?php echo form_error('marca'); ?></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Modelo</label>
                        <input type="text" name="modelo" class="form-control <?php echo form_error('modelo') ? 'is-invalid':''?>" placeholder="Ingrese modelo del vehiculo"  value="<?php echo form_error('modelo') ? set_value('modelo'): $modelo; ?>">
                        <div class="invalid-feedback"><?php echo form_error('modelo'); ?></div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Patente</label>
                        <input type="text" name="patente" class="form-control <?php echo form_error('patente') ? 'is-invalid':''?>" placeholder="Ingrese patente del vehiculo"  value="<?php echo form_error('patente') ? set_value('patente'): $patente; ?>">
                        <div class="invalid-feedback"><?php echo form_error('patente'); ?></div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Modelo</label>
                        <input type="text" name="tanque" class="form-control <?php echo form_error('tanque') ? 'is-invalid':''?>" placeholder="Ingrese tamaño del tanque"  value="<?php echo form_error('tanque') ? set_value('tanque'): $tanque; ?>">
                        <div class="invalid-feedback"><?php echo form_error('tanque'); ?></div>
                    </div>

                    

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success mt-4">Guardar</button>
                    </div>

                </form>
                </div>
            </div>
        </div>

       

    </div>
