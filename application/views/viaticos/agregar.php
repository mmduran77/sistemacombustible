<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0">Nuevo</h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>viaticos">Viaticos</a></li>
                <li class="breadcrumb-item active">Nuevo</li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>viaticos" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row">
        
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Ingrese datos </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                <form action="<?php echo base_url();?>nuevo-viaticos/save" method="POST">
                    <div class="form-group">
                        <label class="form-control-label">Comprobante</label>
                        <input type="text" name="comprobante" class="form-control <?php echo form_error('comprobante') ? 'is-invalid':''?>" placeholder="Comprobante"  value="<?php echo set_value('comprobante'); ?>">
                        <div class="invalid-feedback"><?php echo form_error('comprobante'); ?></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Detalle</label>
                        <input type="text" name="detalle" class="form-control <?php echo form_error('detalle') ? 'is-invalid':''?>" placeholder="Detalle"  value="<?php echo set_value('detalle'); ?>">
                        <div class="invalid-feedback"><?php echo form_error('detalle'); ?></div>
                    </div>
                    <div class="form-group">
                                <label class="form-control-label">Importe</label>
                                <input type="text" name="importe" class="form-control <?php echo form_error('importe') ? 'is-invalid':''?>" placeholder="Importe"  value="<?php echo set_value('importe'); ?>">
                                <div class="invalid-feedback"><?php echo form_error('importe'); ?></div>
                    </div>
                    
                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success mt-4">Guardar</button>
                    </div>

                </form>
                </div>
            </div>
        </div>
        </div>
        