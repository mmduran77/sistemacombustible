<!-- Header -->
<div class="header bg-primary pb-6">
    <div class="container-fluid">
    <div class="header-body">
        <div class="row align-items-center py-4">
        <div class="col-lg-6 col-7">
            <h6 class="h2 text-white d-inline-block mb-0"><?php echo $comprobante; ?></h6>
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>vales">Vales</a></li>
                <li class="breadcrumb-item active"><?php echo $comprobante; ?></li>
            </ol>
            </nav>
        </div>
        <div class="col-lg-6 col-5 text-right">
            <a href="<?php echo base_url(); ?>vales" class="btn btn-sm btn-neutral">Volver</a>
        </div>
        </div>
    </div>
    </div>
</div>

<!-- Page content -->
<div class="container-fluid mt--6">

    <div class="row">
        
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Actualice los datos</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                
                <form action="<?php echo base_url();?>vales/update/<?php echo $id_vales; ?>" method="POST">
                    <div class="form-group">
                        <label class="form-control-label">Comprobante</label>
                        <input type="text" name="comprobante" class="form-control <?php echo form_error('comprobante') ? 'is-invalid':''?>" placeholder="Ingrese comprobante"  value="<?php echo form_error('comprobante') ? set_value('comprobante'): $comprobante; ?>">
                        <div class="invalid-feedback"><?php echo form_error('comprobante'); ?></div>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Litros</label>
                        <input type="text" name="litros" class="form-control <?php echo form_error('litros') ? 'is-invalid':''?>" placeholder="Ingrese Litros"  value="<?php echo form_error('litros') ? set_value('litros'): $litros; ?>">
                        <div class="invalid-feedback"><?php echo form_error('litros'); ?></div>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label">Importe</label>
                        <input type="text" name="importe" class="form-control <?php echo form_error('importe') ? 'is-invalid':''?>" placeholder="Ingrese patente del vehiculo"  value="<?php echo form_error('importe') ? set_value('importe'): $importe; ?>">
                        <div class="invalid-feedback"><?php echo form_error('importe'); ?></div>
                    </div>
                    

                    

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-success mt-4">Guardar</button>
                    </div>

                </form>
                </div>
            </div>
        </div>

       

    </div>
