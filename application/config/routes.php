<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Dashboard';

$route['login'] = 'Signin';
$route['cerrarsesion'] = 'Signin/logout'; 

$route['usuario'] = 'usuario/Lista';
$route['nuevo-usuario'] = 'usuario/Agregar';
$route['nuevo-usuario/save'] = 'usuario/Agregar/save';
$route['usuarios/(:num)'] = 'usuario/Editar/index/$1';
$route['usuarios/update/(:num)'] = 'usuario/Editar/update/$1';
$route['usuario/delete/(:num)'] = 'usuario/Lista/delete/$1';
$route['usuario/imprimir/(:num)']='usuario/impresion/imprimir/$1';

$route['ordenes'] = 'ordenes/Lista';
$route['nuevo-ordenes'] = 'ordenes/Agregar';
$route['nuevo-ordenes/save'] = 'ordenes/Agregar/save';
$route['ordenes/(:num)'] = 'ordenes/Editar/index/$1';
$route['ordenes/update/(:num)'] = 'ordenes/Editar/update/$1';
$route['ordenes/delete/(:num)'] = 'ordenes/Lista/delete/$1';

$route['chat'] = 'chat';
$route['nuevo-correo'] = 'chat/redactar';
$route['nuevo-correo/save'] = 'chat/save';
$route['correo/leer/(:num)']='chat/leer/$1';
$route['correo/delete/(:num)'] = 'chat/delete/$1';


$route['clientes'] = 'cliente/Lista';
$route['nuevo-cliente'] = 'cliente/Agregar';
$route['nuevo-cliente/save'] = 'cliente/Agregar/save';
$route['cliente/(:num)'] = 'cliente/Editar/index/$1';
$route['cliente/update/(:num)'] = 'cliente/Editar/update/$1';
$route['cliente/delete/(:num)'] = 'cliente/Lista/delete/$1';

$route['distancias'] = 'distancias/Lista_dis';
$route['nuevo-distancias'] = 'distancias/Agregar';
$route['nuevo-distancias/save'] = 'distancias/Agregar/save';
$route['distancias/(:num)'] = 'distancias/Editar/index/$1';
$route['distancias/update/(:num)'] = 'distancias/Editar/update/$1';
$route['distancias/delete/(:num)']= 'distancias/Lista_dis/delete/$1';

$route['vales'] = 'vales/Lista';
$route['nuevo-vales'] = 'vales/Agregar';
$route['nuevo-vales/save'] = 'vales/Agregar/save';
$route['vales/(:num)'] = 'vales/Editar/index/$1';
$route['vales/update/(:num)'] = 'vales/Editar/update/$1';
$route['vales/delete/(:num)'] = 'vales/Lista/delete/$1';

$route['viaticos'] = 'viaticos/Lista';
$route['nuevo-viaticos'] = 'viaticos/Agregar';
$route['nuevo-viaticos/save'] = 'viaticos/Agregar/save';
$route['viaticos/(:num)'] = 'viaticos/Editar/index/$1';
$route['viaticos/update/(:num)'] = 'viaticos/Editar/update/$1';
$route['viaticos/delete/(:num)'] = 'viaticos/Lista/delete/$1';

$route['localidades'] = 'localidad/Lista';
$route['nuevo-localidad'] = 'localidad/Agregar';
$route['nuevo-localidad/save'] = 'localidad/Agregar/save';
$route['localidad/(:num)'] = 'localidad/Editar/index/$1';
$route['localidad/update/(:num)'] = 'localidad/Editar/update/$1';
$route['localidad/delete/(:num)'] = 'localidad/Lista/delete/$1';

$route['vehiculos'] = 'vehiculos/Lista';
$route['nuevo-vehiculo'] = 'vehiculos/Agregar';
$route['nuevo-vehiculo/save'] = 'vehiculos/Agregar/save';
$route['nuevo-vehiculo/upload'] = 'vehiculos/Agregar/upload';
$route['vehiculo/(:num)'] = 'vehiculos/Editar/index/$1';
$route['vehiculo/update/(:num)'] = 'vehiculos/Editar/update/$1';
$route['vehiculo/delete/(:num)'] = 'vehiculos/Lista/delete/$1';



$route['perfil'] = 'Profile';
$route['perfil/save'] = 'Profile/save';
$route['perfil/upload'] = 'Profile/upload';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
