
(function(){

    $("tr td #eliminar").click(function(ev){
        ev.preventDefault();
        var nombre=$(this).parents('tr').find('td:first').text();
        var id=$(this).attr('data-id');
        var self=this;
        Swal.fire({
            title: 'ESTAS SEGURO DE ELIMINAR AL USUARIO?'+nombre+'?',
            text: "Estas seguro de queres hacerlo?!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'si',
            cancelButtonText: 'No',
          }).then((result) => {
            if (result.isConfirmed) {
           
                $.ajax({
                   type:'POST',
                   url: 'distancias/delete',
                   data:{'id_distancia':id},

                  
                   success: function(){
                     $(self).parents('tr').remove();
                    Swal.fire(
                        'Eliminado!',
                        'El usuario fue eliminado.',
                        'success'
                     )
                   }
                })
              
            }
          })
    })
   

})();




